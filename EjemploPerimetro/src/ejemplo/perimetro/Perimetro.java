/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo.perimetro;

/**
 * Clase de ejemplo de JUnit + Javadoc
 *
 * @author Matías
 * @version 1.0
 *
 */
public class Perimetro { 
    
     /**
     * Método que suma los lados de una figura
     * 
     * @param int a, int b, int c que son las medidas de los lados
     * @return a + b + c la suma de todos los lados , es decir, el perìmetro
     */
    
    public static int perimetro(int a, int b, int c) {
        return a + b + c;
    }
    
}
