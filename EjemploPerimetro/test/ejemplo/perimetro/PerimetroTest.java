/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo.perimetro;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matías
 */
public class PerimetroTest {

    public PerimetroTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of perimetro method, of class Perimetro.
     */
    
    @Test
    public void testPerimetro() {

        int a = 1;
        int b = 2;
        int c = 3;
        int expResult = 6;
        int result = Perimetro.perimetro(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    @Test
    public void testPerimetro2() {

        int a = 4;
        int b = 5;
        int c = 6;
        int expResult = 15;
        int result = Perimetro.perimetro(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    @Test
    public void testPerimetro3() {

        int a = 7;
        int b = 8;
        int c = 3;
        int expResult = 18;
        int result = Perimetro.perimetro(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    @Test
    public void testPerimetro4() {

        int a = 1;
        int b = 1;
        int c = 2;
        int expResult = 4;
        int result = Perimetro.perimetro(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    @Test
    public void testPerimetro5() {

        int a = 3;
        int b = 2;
        int c = 3;
        int expResult = 8;
        int result = Perimetro.perimetro(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    @Test
    public void testPerimetro6() {

        int a = 15;
        int b = 5;
        int c = 5;
        int expResult = 25;
        int result = Perimetro.perimetro(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    @Test
    public void testPerimetro7() {

        int a = 3;
        int b = 4;
        int c = 3;
        int expResult = 10;
        int result = Perimetro.perimetro(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    @Test
    public void testPerimetro8() {

        int a = 2;
        int b = 2;
        int c = 20;
        int expResult = 24;
        int result = Perimetro.perimetro(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    @Test
    public void testPerimetro9() {

        int a = 5;
        int b = 4;
        int c = 2;
        int expResult = 11;
        int result = Perimetro.perimetro(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    @Test
    public void testPerimetro10() {

        int a = 1;
        int b = 1;
        int c = 1;
        int expResult = 3;
        int result = Perimetro.perimetro(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

}
