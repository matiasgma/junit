/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.ejemplo;

/**
 * Clase de ejemplo de JUnit + Javadoc
 *
 * @author Matías
 * @version 1.0
 *
 */

public class App {

    /**
     * Método que calcula el factorial de un número generado por la clase Math.random
     * 
     * @param num ingresado como parámetro
     * @return int factorial del numero ingresado
     */
    public int calcularFactorial(int num) {
        int factorial = 1;
        for (int i = 1; i <= num; i++) {

            factorial = factorial * i;

        }
        return factorial;
    }
}
