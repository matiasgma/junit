/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.ejemplo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matías
 */
public class AppTest {
    
    public AppTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of sumatoria method, of class App.
     */
    @Test
    public void testSumatoria() {
     
        int[] var = {1, 2, 3, 4, 5};
        App app = new App();
        int expResult = 15;
        // int expResult = 10;
        int result = app.sumatoria(var);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of resta method, of class App.
     */
    @Test
    public void testResta() {
        
        int resta1 = 1;
        int resta2 = 1;
        App instance = new App();
        int expResult = 0;
        int result = instance.resta(resta1, resta2);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    
}
