/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.ejemplo;

/**
 * Clase de ejemplo de JUnit + Javadoc
 *
 * @author Víctor Aravena
 * @version 1.0
 *
 */
public class App {

    /**
     * Método que realiza una suma de todos los números contenidos en una serie
     *
     * @param secuencia Array que contiene una serie de números
     * @return int Resultado de la suma
     */
    public int sumatoria(int secuencia[]) {
        int suma = 0;
        for (int i = 0; i < secuencia.length; i++) {
            suma += secuencia[i];
        }
        return suma;
    }

    /**
     * Método que realiza una resta de dos numero
     *
     * @param resta1
     * @param resta2
     * @return
     */
    public int resta(int resta1, int resta2) {
        return resta1 - resta2;
    }
}
